# Chat Submission Functions

## Testing

The testing infrastructure requires the database emulator to be running. The test script in `package.json` runs
the tests inside the emulator for you, however it seems that firebase swallows standard out. So, to see
the output of tests, run the database emulator with `npx firebase emulators:start --only database`, and then in
a different terminal, run the tests with `npx mocha --exit`
