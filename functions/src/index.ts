import * as admin from 'firebase-admin';

admin.initializeApp();

export { handleNewChat } from './chatSubmission';
