import { SubmissionConfig } from '@zoomsense/chat-submission-types';

/**
 * If `chatContent` is a valid response to `submissionConfig` (i.e.
 * it starts with the specified prefix and has valid content), return
 * the parsed response, else, return undefined
 */
export const parseResponse = (chatContent: string, submissionConfig: SubmissionConfig): string | undefined => {
  if (!submissionConfig.prefix || typeof submissionConfig.prefix !== 'string') return undefined;

  // [whitespace]PREFIX[whitespace]:[whitespace]SUBMISSION
  const pattern = new RegExp(`^\\s*${submissionConfig.prefix}\\s*:\\s*`, 'i');

  // We match case-insensitively
  const match = chatContent.match(pattern);

  if (!match) return undefined;

  // Because we want to retain the capitalisation of the submission,
  // we index into the original submission with the length of the match
  return chatContent.substring(match[0]?.length ?? 0);
};
