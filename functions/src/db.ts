import * as admin from 'firebase-admin';
import '@zoomsense/zoomsense-firebase';
import { ZoomSenseServerFirebaseDB } from '@zoomsense/zoomsense-firebase';
import { Config, Data } from '@zoomsense/chat-submission-types';

declare module '@zoomsense/zoomsense-firebase' {
  interface ZoomSenseConfigCurrentStatePlugins {
    chatsubmission: Config;
  }
  interface ZoomSenseDataPlugins {
    chatSubmission: Data;
  }
}

export const db = new ZoomSenseServerFirebaseDB(admin.database());
