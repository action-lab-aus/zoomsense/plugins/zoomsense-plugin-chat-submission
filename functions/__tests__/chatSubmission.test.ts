import * as assert from 'assert';
import { db } from '../src/db';
import { testRunner } from './setup';
import { handleNewChat } from '../src/chatSubmission';
import { ZoomSenseChat } from '@zoomsense/zoomsense-firebase';

const TEST_MEETING_ID = 'test';

const wrappedHandleNewChat = testRunner.wrap(handleNewChat);

describe('handle new chat', () => {
  it('should work', async () => {
    const timestamp = 1001;
    const chatId = 'chat_id';
    const sensor = 'ZoomSensor_1';
    const section = 0;
    const senderName = 'message sender';
    const senderId = 10;
    const testChat: ZoomSenseChat = {
      msg: 'test prefix: hello!',
      msgSender: senderId,
      msgSenderName: senderName,
      msgReceiver: 0,
      msgReceiverName: 'everyone',
      timestamp: timestamp,
    };

    await db
      .ref(`/config/${TEST_MEETING_ID}/current/currentState/plugins/chatsubmission`)
      .set({ enabled: true, submissions: [{ name: 'test submission', prefix: 'test prefix' }] });
    await db.ref(`/config/${TEST_MEETING_ID}/current/currentSection`).set(section);

    const snap = testRunner.database.makeDataSnapshot(testChat, `/data/chats/${TEST_MEETING_ID}/${sensor}/${chatId}`);

    await wrappedHandleNewChat(snap as any, { params: { meetingId: TEST_MEETING_ID, sensor, chatId } });

    const chatSubmissions = (await db.ref(`/data/plugins/chatSubmission/${TEST_MEETING_ID}`).get()).val();

    assert(chatSubmissions);
    assert.equal(Object.entries(chatSubmissions).length, 1);
    const submission = Object.values(chatSubmissions)[0]!;
    assert.equal(submission.section, section);
    assert.equal(submission.timestamp, timestamp);
    assert.equal(submission.sensor, sensor);
    assert.equal(submission.senderName, senderName);
    assert.equal(submission.senderId, senderId);
    assert.equal(submission.submission, 'hello!');
  });

  it('should do nothing when not enabled', async () => {
    await db
      .ref(`/config/${TEST_MEETING_ID}/current/currentState/plugins/chatsubmission`)
      .set({ enabled: false, submissions: [{ name: 'test submission', prefix: 'test prefix' }] });

    const testChat: ZoomSenseChat = {
      msg: 'test prefix: hmm',
      msgReceiverName: '',
      msgSenderName: '',
      msgReceiver: 0,
      msgSender: 0,
      timestamp: 0,
    };

    const snap = testRunner.database.makeDataSnapshot(testChat, `/data/chats/${TEST_MEETING_ID}/ZoomSensor_1/wowee`);

    await wrappedHandleNewChat(snap as any, {
      params: { meetingId: TEST_MEETING_ID, sensor: 'ZoomSensor_1', chatId: 'wowee' },
    });

    const chatSubmissions = (await db.ref(`/data/plugins/chatSubmission/${TEST_MEETING_ID}`).get()).val();

    assert.equal(chatSubmissions, null);
  });

  it('should accept entries with weird capitalisation', async () => {
    const testChat: ZoomSenseChat = {
      msg: 'TeSt pRefIx: hmm',
      msgReceiverName: '',
      msgSenderName: '',
      msgReceiver: 0,
      msgSender: 0,
      timestamp: 0,
    };

    await db
      .ref(`/config/${TEST_MEETING_ID}/current/currentState/plugins/chatsubmission`)
      .set({ enabled: true, submissions: [{ name: 'test submission', prefix: 'test prefix' }] });
    await db.ref(`/config/${TEST_MEETING_ID}/current/currentSection`).set(0);

    const snap = testRunner.database.makeDataSnapshot(testChat, `/data/chats/${TEST_MEETING_ID}/ZoomSensor_1/a`);

    await wrappedHandleNewChat(snap as any, {
      params: { meetingId: TEST_MEETING_ID, sensor: 'ZoomSensor_1', chatId: 'a' },
    });

    const chatSubmissions = (await db.ref(`/data/plugins/chatSubmission/${TEST_MEETING_ID}`).get()).val();

    assert(chatSubmissions);
    assert.equal(Object.entries(chatSubmissions).length, 1);
    const submission = Object.values(chatSubmissions)[0]!;
    assert.equal(submission.submission, 'hmm');
  });

  it('should do nothing for bad prefixes', async () => {
    const testChat: ZoomSenseChat = {
      msg: 'bad prefix: hmm',
      msgReceiverName: '',
      msgSenderName: '',
      msgReceiver: 0,
      msgSender: 0,
      timestamp: 0,
    };

    await db
      .ref(`/config/${TEST_MEETING_ID}/current/currentState/plugins/chatsubmission`)
      .set({ enabled: true, submissions: [{ name: 'test submission', prefix: 'test prefix' }] });
    await db.ref(`/config/${TEST_MEETING_ID}/current/currentSection`).set(0);

    const snap = testRunner.database.makeDataSnapshot(testChat, `/data/chats/${TEST_MEETING_ID}/ZoomSensor_1/a`);

    await wrappedHandleNewChat(snap as any, {
      params: { meetingId: TEST_MEETING_ID, sensor: 'ZoomSensor_1', chatId: 'a' },
    });

    const chatSubmissions = (await db.ref(`/data/plugins/chatSubmission/${TEST_MEETING_ID}`).get()).val();

    assert.equal(chatSubmissions, null);
  });

  it("don't accept empty submissions", async () => {
    const testChat: ZoomSenseChat = {
      msg: 'test prefix:   ',
      msgReceiverName: '',
      msgSenderName: '',
      msgReceiver: 0,
      msgSender: 0,
      timestamp: 0,
    };

    await db
      .ref(`/config/${TEST_MEETING_ID}/current/currentState/plugins/chatsubmission`)
      .set({ enabled: true, submissions: [{ name: 'test submission', prefix: 'test prefix' }] });
    await db.ref(`/config/${TEST_MEETING_ID}/current/currentSection`).set(0);

    const snap = testRunner.database.makeDataSnapshot(testChat, `/data/chats/${TEST_MEETING_ID}/ZoomSensor_1/a`);

    await wrappedHandleNewChat(snap as any, {
      params: { meetingId: TEST_MEETING_ID, sensor: 'ZoomSensor_1', chatId: 'a' },
    });

    const chatSubmissions = (await db.ref(`/data/plugins/chatSubmission/${TEST_MEETING_ID}`).get()).val();

    assert.equal(chatSubmissions, null);
  });
});
