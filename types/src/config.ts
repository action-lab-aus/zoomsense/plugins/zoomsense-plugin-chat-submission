import { ZoomSensePlugin } from '@zoomsense/zoomsense-firebase';
/// The config for this plugin
export type Config = {
  submissions?: SubmissionConfig[];
} & ZoomSensePlugin;

/// Config for a submission to accept
export type SubmissionConfig = {
  prefix?: string;
  name?: string;
};
