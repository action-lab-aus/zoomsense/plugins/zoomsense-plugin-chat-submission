// A users submission
export type Submission = {
  // The section within the meeting this submission was submitted during
  section: number;
  // When the submission was submitted
  timestamp: number;
  // The sensor that received the submission
  sensor: string;
  // Id of the submitting user
  senderId: number;
  // Name of the submitting user
  senderName: string;
  // The prefix used for the submission (this comes from the plugin's config)
  submissionPrefix: string;
  // The name of the submission (this comes from the plugin's config)
  submissionName: string;
  // The content of the submission
  submission: string;
};
