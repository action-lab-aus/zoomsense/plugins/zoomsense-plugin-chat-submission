import { Submission } from "./submission";

export type Data = {
  [meetingId: string]: {
    [submissionId: string]: Submission;
  };
};
